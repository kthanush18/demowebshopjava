package com.Pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class ComputersPage extends BrowserUtility {

	private static final By DESKTOP_ICON_LOCATOR = By.xpath("//img [@title = 'Show products in category Desktops']");

	public boolean isDesktopIconDisplayed() {
		return isElementVisible(DESKTOP_ICON_LOCATOR);
	}
}
