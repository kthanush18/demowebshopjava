package com.Pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class LandingPage extends BrowserUtility {

	private static final By login_Link_Locator = By.xpath("//a [text() = 'Log in']");

    //Method for navigating to login page and creating instance
    public LoginPage NavigateToLoginPage()
    {
        clickOn(login_Link_Locator);
        return new LoginPage();
    }
}
