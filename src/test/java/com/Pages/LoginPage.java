package com.Pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;
import com.web.ReadMyProperties;

public class LoginPage extends BrowserUtility {
	
	private static final By WELCOME_HEADING_LOCATOR = By.tagName("h1");
	private static final By USERNAME_TEXTBOX_LOCATOR = By.id("Email");
    private static final By PASSWORD_TEXTBOX_LOCATOR = By.id("Password");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//input [@value = 'Log in']");
    private static final String USERNAME = ReadMyProperties.getMyProperty("username");
    private static final String PASSWORD = ReadMyProperties.getMyProperty("password");
    private static final By LOGOUT_LINK_LOCATOR = By.xpath("//a[@class='ico-logout']");
    
    private static final By BOOKS_LINK_LOCATOR = By.xpath("//a[@href='/books']");
    private static final By COMPUTERS_LINK_LOCATOR = By.xpath("//a[@href='/computers']");
    private static final By ELECTRONICS_LINK_LOCATOR = By.xpath("//a[@href='/electronics']");
    
  //Returns welcome message
    public String getWelcomeMessage()
    {
        return getText(WELCOME_HEADING_LOCATOR);
    }

    //Method for user login and creating dashboard instance
    public void enterCredentialsAndLogin()
    {
        enterText(USERNAME_TEXTBOX_LOCATOR, USERNAME);
        enterText(PASSWORD_TEXTBOX_LOCATOR, PASSWORD);
        clickOn(LOGIN_BUTTON_LOCATOR);
    }
    
    public boolean isLogoutLinkDisplayed() {
		return isElementVisible(LOGOUT_LINK_LOCATOR);
	}
    
    public BooksPage navigateToBooksPage() {
    	clickOn(BOOKS_LINK_LOCATOR);
		return new BooksPage();
    }
    
    public ComputersPage navigateToComputerspage() {
    	clickOn(COMPUTERS_LINK_LOCATOR);
		return new ComputersPage();
    }
    
    public ElectronicsPage navigateToElectronicsPage() {
    	clickOn(ELECTRONICS_LINK_LOCATOR);
		return new ElectronicsPage();
    }

}
