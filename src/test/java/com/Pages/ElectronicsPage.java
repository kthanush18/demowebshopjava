package com.Pages;

import org.openqa.selenium.By;

import com.web.BrowserUtility;

public class ElectronicsPage extends BrowserUtility {

	private static final By CELLPHONES_ICON_LOCATOR = By.xpath("//img [@title = 'Show products in category Cell phones']");

	public boolean isCellPhonesIconDisplayed() {
		return isElementVisible(CELLPHONES_ICON_LOCATOR);
	}
}
