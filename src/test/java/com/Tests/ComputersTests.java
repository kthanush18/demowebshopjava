package com.Tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.Pages.LandingPage;
import com.web.ReadMyProperties;

public class ComputersTests extends TestBase{

	@Test
	public void LoadingOfComputersPage() {
		_driver.get(ReadMyProperties.getMyProperty("URL"));
		LandingPage landingPage = new LandingPage();
		loginPage = landingPage.NavigateToLoginPage();
		loginPage.enterCredentialsAndLogin();
		
		computersPage = loginPage.navigateToComputerspage();
		AssertJUnit.assertTrue(computersPage.isDesktopIconDisplayed());
	}
}
