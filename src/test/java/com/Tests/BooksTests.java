package com.Tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.Pages.LandingPage;
import com.web.ReadMyProperties;

public class BooksTests extends TestBase {
	
	@Test
	public void LoadingOfBooksPage() {
		_driver.get(ReadMyProperties.getMyProperty("URL"));
		LandingPage landingPage = new LandingPage();
		loginPage = landingPage.NavigateToLoginPage();
		loginPage.enterCredentialsAndLogin();
		
		booksPage = loginPage.navigateToBooksPage();
		AssertJUnit.assertTrue(booksPage.isSortDropdownDisplayed());
	}

}
