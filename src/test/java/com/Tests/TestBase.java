package com.Tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.Pages.BooksPage;
import com.Pages.ComputersPage;
import com.Pages.ElectronicsPage;
import com.Pages.LoginPage;
import com.web.BrowserType;
import com.web.BrowserUtility;

public class TestBase extends BrowserUtility {

	protected LoginPage loginPage;
	protected BooksPage booksPage;
	protected ComputersPage computersPage;
	protected ElectronicsPage electronicsPage;
	
	
	@BeforeMethod
	public void launchBrowser() {
		launchLocalBrowser(BrowserType.CHROME);
	}
	
	@AfterMethod
	public void tearDown() {
		quitBrowser();
	}
}
