package com.Tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.Pages.LandingPage;
import com.web.ReadMyProperties;

public class ElectronicsTests extends TestBase{
	
	@Test
	public void LoadingOfElectronicsPage() {
		_driver.get(ReadMyProperties.getMyProperty("URL"));
		LandingPage landingPage = new LandingPage();
		loginPage = landingPage.NavigateToLoginPage();
		loginPage.enterCredentialsAndLogin();
		
		electronicsPage = loginPage.navigateToElectronicsPage();
		AssertJUnit.assertTrue(electronicsPage.isCellPhonesIconDisplayed());
	}

}
