package com.Tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import com.Pages.LandingPage;
import com.web.ReadMyProperties;

public class LoginPageTests extends TestBase {

	@Test
	public void loginToDemoWebShop() {
		_driver.get(ReadMyProperties.getMyProperty("URL"));
		LandingPage landingPage = new LandingPage();
		loginPage = landingPage.NavigateToLoginPage();
		loginPage.enterCredentialsAndLogin();
		AssertJUnit.assertTrue(loginPage.isLogoutLinkDisplayed());
	}
	
}
